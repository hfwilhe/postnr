import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostnrFormComponent } from './postnr-form.component';

describe('PostnrFormComponent', () => {
  let component: PostnrFormComponent;
  let fixture: ComponentFixture<PostnrFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostnrFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostnrFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
